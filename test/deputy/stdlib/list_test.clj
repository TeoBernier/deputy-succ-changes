(ns deputy.stdlib.list-test
  (:require
   [deputy.core :refer [defterm]]
   [clojure.test :refer [deftest testing is]]
   [deputy.utils :as u :refer [ko-expr?]]
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.stdlib.nat :as nat]
   [deputy.stdlib.list :refer :all]
   [deputy.extensions.desc :as d]))

(defterm [t0 (llist nat/nat)]
  (lnil nat/nat))

(defterm [t1 (llist nat/nat)]
  (lcons nat/nat nat/one t0))

(defterm [t2 (llist nat/nat)]
  (lcons nat/nat nat/two t1))

(defterm [t3 (llist nat/nat)]
  (lcons nat/nat nat/three t2))

(deftest eval
  (testing "case"
    (is (n/beta-equiv
         (app lcase nat/nat t0 (λ [_] nat/nat) nat/three (λ [x y] nat/four))
         nat/three))
    (is (n/beta-equiv
         (app lcase nat/nat t1  (λ [_] nat/nat) nat/three (λ [x y] nat/four))
         nat/four))
    (is (n/beta-equiv
         (app lcase nat/nat t2 (λ [_] nat/nat) nat/three (λ [x y] nat/four))
         nat/four))
    (is (n/beta-equiv
         (app lcase nat/nat t3 (λ [_] nat/nat) nat/three (λ [x y] nat/four))
         nat/four)))

  (testing "append"
    (is (n/beta-equiv
         (app append nat/nat t0 t0)
         t0))
    (is (n/beta-equiv
         (app append nat/nat t0 t1)
         t1))
    (is (n/beta-equiv
         (app append nat/nat t1 t0)
         t1))
    (is (n/beta-equiv
         (app append nat/nat t3 t0)
         t3))
    (is (n/beta-equiv
         (app append nat/nat t0 t3)
         t3))
    (is (n/beta-equiv
         (app append nat/nat t2 t3)
         (app lcons 
          nat/nat nat/two 
          (lcons 
           nat/nat nat/one
           (lcons 
            nat/nat nat/three
            (lcons
             nat/nat nat/two
             (lcons
              nat/nat nat/one 
              (lnil nat/nat))))))))))
