(ns deputy.stdlib.btree-test
  (:require
   [deputy.core :refer [defterm]]
   [clojure.test :refer [deftest testing is]]
   [deputy.utils :as u :refer [ko-expr?]]
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.stdlib.nat :as nat]
   [deputy.stdlib.list :as list]
   [deputy.stdlib.btree :refer :all]
   [deputy.extensions.desc :as d]))

(defterm [t0 (tree nat/nat nat/nat)]
  (leaf nat/nat nat/nat nat/ze))

(defterm [t1 (tree nat/nat nat/nat)]
  (leaf nat/nat nat/nat nat/one))

(defterm [t2 (tree nat/nat nat/nat)]
  (node nat/nat nat/nat t0 nat/two t1))

(defterm [t3 (tree nat/nat nat/nat)]
  (leaf nat/nat nat/nat nat/three))

(defterm [t4 (tree nat/nat nat/nat)]
  (node nat/nat nat/nat t2 nat/four t3))

(deftest eval
  (testing "case"
    (is (n/beta-equiv
         (app tcase nat/nat nat/nat t0 (λ [_] nat/nat) (fun [_] nat/three) (λ [x y z] nat/four))
         nat/three))
    (is (n/beta-equiv
         (app tcase nat/nat nat/nat t0 (λ [_] nat/nat) (fun [x] x) (λ [x y z] nat/four))
         nat/ze))
    (is (n/beta-equiv
         (app tcase nat/nat nat/nat t1  (λ [_] nat/nat) (fun [_] nat/three) (λ [x y z] nat/four))
         nat/three))
    (is (n/beta-equiv
         (app tcase nat/nat nat/nat t1  (λ [_] nat/nat) (fun [x] x) (λ [x y z] nat/four))
         nat/one))
    (is (n/beta-equiv
         (app tcase nat/nat nat/nat t2 (λ [_] nat/nat) (fun [_] nat/three) (λ [x y z] nat/four))
         nat/four))
    (is (n/beta-equiv
         (app tcase nat/nat nat/nat t2 (λ [_] nat/nat) (fun [_] nat/three) (λ [x y z] x))
         t0))
    (is (n/beta-equiv
         (app tcase nat/nat nat/nat t2 (λ [_] nat/nat) (fun [_] nat/three) (λ [x y z] z))
         t1))
    (is (n/beta-equiv
         (app tcase nat/nat nat/nat t2 (λ [_] nat/nat) (fun [_] nat/three) (λ [x y z] y))
         nat/two))
    (is (n/beta-equiv
         (app tcase nat/nat nat/nat t3 (λ [_] nat/nat) (fun [_] nat/three) (λ [x y z] nat/four))
         nat/three))
    (is (n/beta-equiv
         (app tcase nat/nat nat/nat t4 (λ [_] nat/nat) (fun [_] nat/three) (λ [x y z] nat/four))
         nat/four)))

  (testing "flatten"
    (is (n/beta-equiv
         (app flatten nat/nat nat/nat t0)
         (app list/lcons nat/nat nat/ze (list/lnil nat/nat))))
    (is (n/beta-equiv
         (app flatten nat/nat nat/nat t1)
         (app list/lcons nat/nat nat/one (list/lnil nat/nat))))
    (is (n/beta-equiv
         (app flatten nat/nat nat/nat t2)
         (app list/lcons nat/nat nat/ze (list/lcons nat/nat nat/one (list/lnil nat/nat)))))
    (is (n/beta-equiv
         (app flatten nat/nat nat/nat t3)
         (app list/lcons nat/nat nat/three (list/lnil nat/nat))))
    (is (n/beta-equiv
         (app flatten nat/nat nat/nat t4)
         (app list/lcons
              nat/nat nat/ze
              (list/lcons 
               nat/nat nat/one
               (list/lcons nat/nat nat/three
                           (list/lnil nat/nat))))))))
