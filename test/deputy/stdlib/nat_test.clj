(ns deputy.stdlib.nat-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [deputy.utils :as u :refer [ko-expr?]]
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.stdlib.nat :refer :all]
   [deputy.extensions.desc :as d]))

(deftest typing
  (testing "natD : desc"
    (is (n/beta-equiv 
         d/descD
         (type-synth natD))))

  (testing "nat : :type"
    (is (n/beta-equiv 
         :type
         (type-synth nat))))

  (testing "ze : nat"
    (is (n/beta-equiv 
         nat
         (type-synth ze))))

  (testing "su : (=> nat nat)"
    (is (n/beta-equiv 
         (=> nat nat)
         (type-synth su))))

  (testing "two : nat"
    (is (n/beta-equiv
         nat
         (type-synth two))))

  (testing "three : nat"
    (is (n/beta-equiv
         nat
         (type-synth three))))

  (testing "four : nat"
    (is (n/beta-equiv
         nat
         (type-synth four))))

  (testing "five : nat"
    (is (n/beta-equiv
         nat
         (type-synth five))))

  (testing "six : nat"
    (is (n/beta-equiv
         nat
         (type-synth six))))

  (testing "pred : (=> nat nat)"
    (is (n/beta-equiv
         (=> nat nat)
         (type-synth pred))))

  (testing "ind : (...)"
    (is (n/beta-equiv
         (Π [n nat]
            (Π [P (=> nat :type)]
               (=> (P ze)
                   (=> (Π [n nat]
                          (=> (P n)
                              (P (su n))))
                       (P n)))))
         (type-synth ind))))

  (testing "add : (=> nat (=> nat nat))"
    (is (n/beta-equiv
         (=> nat (=> nat nat))
         (type-synth add)))))

(deftest eval
  (testing "case"
    (is (n/beta-equiv
         (app ncase ze (λ [_] nat) three (λ [_] four))
         three))
    (is (n/beta-equiv
         (app ncase one (λ [_] nat) three (λ [_] four))
         four))
    (is (n/beta-equiv
         (app ncase two (λ [_] nat) three (λ [_] four))
         four))
    (is (n/beta-equiv
         (app ncase three (λ [_] nat) three (λ [_] four))
         four)))

  (testing "pred"
    (is (n/beta-equiv
         (app pred ze)
         ze))
    (is (n/beta-equiv
         (app pred (su ze))
         ze))
    (is (n/beta-equiv
         (app pred (su (su ze)))
         (app su ze)))
    (is (n/beta-equiv
         (app pred (su (su (su ze))))
         (app su (su ze)))))

  (testing "add"
    (is (n/beta-equiv
         (app add two three)
         five))
    (is (n/beta-equiv
         (app add three two)
         five))
    (is (n/beta-equiv
         (app add two two)
         four))
    (is (n/beta-equiv
         (app add three three)
         six))

    (is (n/beta-equiv
        (n/evaluate (app (fun [k x] (add k x)) three one))
        four))
    
    (is (n/beta-equiv
         (n/evaluate (app (fun [k x] (add x k)) three one))
         four)))

  (testing "mult"
    (is (n/beta-equiv
         (app mult ze ze)
         ze))
    (is (n/beta-equiv
         (app mult ze two)
         ze))
    (is (n/beta-equiv
         (app mult two ze)
         ze))
    (is (n/beta-equiv
         (app mult one two)
         two))
    (is (n/beta-equiv
         (app mult two one)
         two))
    (is (n/beta-equiv
         (app mult two three)
         six))
    (is (n/beta-equiv
         (app mult three two)
         six)))

  (testing "fact"
    (is (n/beta-equiv
         (n/evaluate (app fact two))
         two))

    (is (n/beta-equiv
         (n/evaluate (app fact three))
         six))))





