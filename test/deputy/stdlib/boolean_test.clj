(ns deputy.stdlib.boolean-test
  (:require
   [clojure.test :refer [deftest testing is]]
   ;;[deputy.utils :as u :refer [ko-expr?]]
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.stdlib.boolean :refer :all]
   ;;[deputy.extensions.enum :as e :refer [switch enum index succ]]
))

(deftest typing
  (testing "bool : :type"
    (is (n/beta-equiv 
         :type
         (type-synth bool))))

  (testing "tt : bool"
    (is (n/beta-equiv
         bool
         (type-synth tt))))

  (testing "ff : bool"
    (is (n/beta-equiv
         bool
         (type-synth ff))))
  
  (testing "ifte : Π [b bool][P (=> bool :type)] (=> (P tt) (P ff) (P b))"
    (is (n/beta-equiv
         (Π [b (clj bool)] 
            (Π [P (=> (clj bool) :type)]
               (=> (P (clj tt))
                   (=> (P (clj ff))
                       (P b)))))
         (type-synth ifte)))))

(deftest eval
  (testing "ifte tt P e1 e2 ↝ e1"
    (is (binding [s/*allow-free-variables* true]
          (n/beta-equiv
           (app (clj ifte) (clj tt) P e1 e2)
           (dvar e1)))))
  (testing "ifte ff P e1 e2 ↝ e2"
    (is (binding [s/*allow-free-variables* true]
          (n/beta-equiv
           (app (clj ifte) (clj ff) P e1 e2)
           (dvar e2))))))

