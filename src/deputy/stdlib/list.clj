(ns deputy.stdlib.list
  (:require
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.core :refer [defterm]]
   [deputy.extensions.labels :as l :refer [tag ls cons-l vls]]
   [deputy.extensions.enum :as e :refer [switch struct enum index succ]]
   [deputy.extensions.fix :as r]
   [deputy.extensions.desc :as d]
   [deputy.extensions.convert :as c]))

(defterm [llistD (=> :type d/descD)]
  (λ [A]
     (d/dsig-struct
      (vls :lnil :lcons)
      [(d/dk :unit)
       (d/dprod (d/dk A) (d/dprod d/dx (d/dk :unit)))
       nil])))

(defterm [llist (=> :type :type)]
  (λ [A] (d/mu (llistD A))))

(defterm [lnil (Π [A :type] (llist A))]
  (λ [A] (d/ctor [zero nil])))

(defterm [lcons (Π [A :type] (=> A (=> (llist A) (llist A))))]
  (fun [A a as] (d/ctor [(succ zero) [a [as nil]]])))

(defterm [lcase (Π [A :type]
                   (Π [as (llist A)]
                      (Π [P (=> (llist A) :type)]
                         (=> (P (lnil A))
                             (=> (Π [a A]
                                    (Π [as (llist A)]
                                       (P (lcons A a as))))
                                 (P as))))))]
  (fun [A as P cn cc]
       (c/convert!
        ((switch (π1 (d/inner as))
                 as x
                 return (Π [xs (d/interp (d/switch-desc x with
                                                        [(d/dk :unit)
                                                         (d/dprod (d/dk A) (d/dprod d/dx (d/dk :unit)))
                                                         nil])
                                         (llist A))]
                           (P (d/ctor [x xs])))
                 with
                 [(fun [tt] (c/convert! cn
                                        from (P (d/ctor [zero nil]))
                                        to (P (d/ctor [zero tt]))))
                  (fun [nk] (c/convert! (cc (π1 nk) (π1 (π2 nk)))
                                        from (P (lcons A (π1 nk) (π1 (π2 nk))))
                                        to (P (d/ctor [(succ zero) nk]))))
                  nil])
         (π2 (d/inner as)))
        from (P (d/ctor [(π1 (d/inner as)) (π2 (d/inner as))]))
        to (P as))))

(defterm [ind (Π [A :type]
                 (Π [as (llist A)]
                    (Π [P (=> (llist A) :type)]
                       (=> (P (lnil A))
                           (=> (Π [a A]
                                  (Π [as (llist A)]
                                     (=> (P as)
                                         (P (lcons A a as)))))
                               (P as))))))]
  (fun [A as P bc ih]
       (r/fix (llist A)
              P
              as
              (fun [as]
                   (lcase A as
                          (λ [as] (r/rec (llist A) P (P as)))
                          (r/ret bc)
                          (fun [a ask] (r/rec-call ask (fun [Pask]
                                       (r/ret (ih a ask Pask))))))))))


(defterm [append (Π [A :type] (=> (llist A) (=> (llist A) (llist A))))]
  (fun [A xs ys]
       (ind A xs
            (fun [_] (llist A))
            ys
            (fun [a _ xsys] (lcons A a xsys)))))
