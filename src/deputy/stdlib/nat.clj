(ns deputy.stdlib.nat
  (:require
   [deputy.utils :as u :refer [example examples]]
   [deputy.syntax :as s :refer :all :exlude [+examples-enabled+]]
   [deputy.unparse :as up]
   [deputy.ast :as a]
   [deputy.certified :as cert]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.core :refer [defterm try-defterm last-error]]
   [deputy.extensions.labels :as l :refer [tag ls cons-l vls]]
   [deputy.extensions.enum :as e :refer [switch struct enum index succ]]
   [deputy.extensions.fix :as r]
   [deputy.extensions.desc :as d]
   [deputy.extensions.convert :as c]))

(def ^:private +examples-enabled+ true)

(defterm [natD d/descD]
  (d/dsig-struct
   (vls :ze :su)
   [(d/dk :unit)
    (d/dprod d/desc-x (d/dk :unit))
    nil]))

(example
 (a/unparse natD) 
 => 'natD)
 
(defterm [nat :type]
  (d/mu natD))

(examples
 (a/unparse nat) => 'nat

 ;; this is not good:

 (a/unparse (n/evaluate nat))
 => :deputy.stdlib.nat/nat
)

(defterm [ze nat]
  (d/ctor [zero nil]))

(up/register-unparse-pattern! ::ze '(ctor [zero nil]) (fn [_ _] 0))

(examples
 (a/unparse ze) => 'ze

 (a/unparse (n/evaluate ze)) => 0
)

(defterm [su [n nat] nat]
  (d/ctor [(succ zero) [n nil]]))

(up/register-unparse-pattern! ::su '(ctor [(succ zero) [?n nil]]) 
                              (fn [subst _] (let [arg (get subst '?n)]
                                              (if (int? arg)
                                                (inc arg)
                                                (list 'su arg)))))

(examples
 (a/unparse su) => 'su

 (a/unparse (app su ze)) => '(su ze)

 (a/unparse (n/evaluate (app su ze)))
 => 1

 (a/unparse (n/evaluate (app su (su (su (su (su ze)))))))
 => 5
 )

(defterm [one nat]
  (su ze))

(defterm [two nat]
  (su one))

(defterm [three nat]
  (su two))

(defterm [four nat]
  (su three))

(defterm [five nat]
  (su four))

(defterm [six nat]
  (su five))

(examples
 (a/unparse six) => 'six
 
 (a/unparse (n/evaluate six)) => 6 
)

(defterm [ncase [n nat] [P (=> nat :type)]
          [cz (P ze)]
          [cs (Π [k nat]
                 (P (su k)))]
          (P n)]
  (c/convert!
   ((switch (π1 (d/inner n))
            as x
            return (Π [xs (d/interp (d/switch-desc x with
                                                   [(d/dk :unit)
                                                    (d/dprod d/desc-x (d/dk :unit))
                                                    nil])
                                    nat)]
                      (P (d/ctor [x xs])))
            with
            [(fun [tt] (c/convert! cz
                                   from (P (d/ctor [zero nil]))
                                   to (P (d/ctor [zero tt]))))
             (fun [nk] (c/convert! (cs (π1 nk))
                                   from (P (su (π1 nk)))
                                   to (P (d/ctor [(succ zero) nk]))))
             nil])
    (π2 (d/inner n)))
   from (P (d/ctor [(π1 (d/inner n)) (π2 (d/inner n))]))
   to (P n)))

(defterm [pred [n nat] nat]
  (ncase n (λ [_] nat) ze (fun [nk] nk)))

(defterm [ind [n nat] [P (=> nat :type)]
          [bc (P ze)]
          [ih (Π [k nat]
                 (=> (P k)
                     (P (su k))))]
          (P n)] 
  (r/fix nat P n
         (fun [n]
              (ncase 
               n
               (λ [n] (r/rec nat P (P n)))
               (r/ret bc)
               (fun [nk] 
                    (r/rec-call nk (fun [Pnk] (r/ret (ih nk Pnk)))))))))

(defterm [add [m nat] [n nat] nat]
  (ind
   n
   (fun [n] nat)
   m
   (fun [n mn] (su mn))))

(comment
(defterm [ADD [m nat] [n nat] :type] nat)

(defterm [add2 [m nat] [n nat] (ADD m n)]
  (ind
   n
   (fun [n] nat)
   m
   (fun [n mn] (su (?hole mn)))))



)

(defterm [mult [m nat] [n nat] nat]
  (ind n (fun [_] nat)
       ze
       (fun [n multmn] (add m multmn))))

(defterm [double [k nat] nat]
  (app add k k))

(defterm [fact [n nat] nat]
  (ind n (fun [_] nat)
       one
       (fun [n factn] (mult factn (su n)))))

(comment
  (a/unparse (n/evaluate (app fact three)))
  ;; => 6
)
 
