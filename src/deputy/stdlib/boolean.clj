(ns deputy.stdlib.boolean
  (:require
   [deputy.syntax :as s :refer [fun the Π =>]]
   [deputy.ast :as a]
   [deputy.unparse :as up]
   [deputy.core :refer [defterm]]
   [deputy.norm :as n]
   [deputy.extensions.labels :refer [vls]]
   [deputy.extensions.enum :as e :refer [switch enum succ]]))

(defterm [bool :type]
  (enum (vls :false  :true)))

(a/unparse (n/evaluate bool))


(up/register-unparse-pattern! ::bool '(enum (ls :false :true)) (fn [_ _] 'bool))

(defterm [ff bool] zero)

(up/register-unparse-pattern! ::ff 'zero (fn [_ _] 'ff))

(defterm [tt bool] (succ zero))

(up/register-unparse-pattern! ::tt '(succ zero) (fn [_ _] 'tt))


(defterm [ifte [b bool] [P (=> bool :type)] [e1 (P tt)] [e2 (P ff)] (P b)]
  (switch b as x return (P x) with [e2 e1 nil]))

;; XXX: do we unparse like this ?
;; (switch ?b as x return ?Px with [?e2 ?e1 nil])
;; ==> (if ?b [x ?Px] ?e1 ?e2)

