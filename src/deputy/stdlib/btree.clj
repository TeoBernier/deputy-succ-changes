(ns deputy.stdlib.btree
  (:require
   [deputy.syntax :as s :refer :all]
   [deputy.ast :as a]
   [deputy.norm :as n]
   [deputy.typing :refer [type-check type-synth]]
   [deputy.core :refer [defterm]]
   [deputy.extensions.labels :as l :refer [tag ls cons-l vls]]
   [deputy.extensions.enum :as e :refer [switch struct enum index succ]]
   [deputy.extensions.fix :as r]
   [deputy.extensions.desc :as d]
   [deputy.extensions.convert :as c]
   [deputy.stdlib.nat :as nat]
   [deputy.stdlib.list :as list]))

(defterm [treeD (=> :type (=> :type d/descD))]
  (λ [A B]
     (d/dsig-struct
      (vls :leaf :node)
      [(d/dprod (d/dk A) (d/dk :unit))
       (d/dprod d/dx (d/dprod (d/dk B) (d/dprod d/dx (d/dk :unit))))
       nil])))

(defterm [tree (=> :type (=> :type :type))]
  (λ [A B] (d/mu (treeD A B))))

(defterm [leaf (Π [A B :type] (=> A (tree A B)))]
  (λ [A B a] (d/ctor [zero [a nil]])))

(defterm [node (Π [A B :type] (=> (tree A B) (=> B (=> (tree A B) (tree A B)))))]
  (fun [A B ls b rs] (d/ctor [(succ zero) [ls [b [rs nil]]]])))

(defterm [tcase (Π [A B :type]
                   (Π [t (tree A B)]
                      (Π [P (=> (tree A B) :type)]
                         (=> (Π [a A] (P (leaf A B a)))
                             (=> (Π [ls (tree A B)]
                                    (Π [b B]
                                       (Π [rs (tree A B)]
                                          (P (node A B ls b rs)))))
                                 (P t))))))]
  (fun [A B t P cl cn]
       (c/convert!
        ((switch (π1 (d/inner t))
                 as x
                 return (Π [xs (d/interp (d/switch-desc x with
                                                        [(d/dprod (d/dk A) (d/dk :unit))
                                                         (d/dprod d/dx (d/dprod (d/dk B) (d/dprod d/dx (d/dk :unit))))
                                                         nil])
                                         (tree A B))]
                           (P (d/ctor [x xs])))
                 with
                 [(fun [args] (c/convert! (cl (π1 args))
                                        from (P (leaf A B (π1 args)))
                                        to (P (d/ctor [zero args]))))
                  (fun [args] (c/convert! (cn (π1 args) (π1 (π2 args)) (π1 (π2 (π2 args))))
                                        from (P (node A B (π1 args) (π1 (π2 args)) (π1 (π2 (π2 args)))))
                                        to (P (d/ctor [(succ zero) args]))))
                  nil])
         (π2 (d/inner t)))
        from (P (d/ctor [(π1 (d/inner t)) (π2 (d/inner t))]))
        to (P t))))

(defterm [ind (Π [A B :type]
                 (Π [t (tree A B)]
                    (Π [P (=> (tree A B) :type)]
                       (=> (Π [a A] (P (leaf A B a)))
                           (=> (Π [ls (tree A B)]
                                  (Π [b B]
                                     (Π [rs (tree A B)]
                                        (=> (P ls)
                                            (P rs)
                                            (P (node A B ls b rs))))))
                               (P t))))))]
  (fun [A B t P bc ih]
       (r/fix (tree A B)
              P
              t
              (fun [t]
                   (tcase A B t
                          (λ [t] (r/rec (tree A B) P (P t)))
                          (λ [a] (r/ret (bc a)))
                          (fun [ls b rs]
                               (r/rec-call ls (fun [Pls]
                               (r/rec-call rs (fun [Prs]
                               (r/ret (ih ls b rs Pls Prs))))))))))))

(defterm [flatten (Π [A B :type]
                     (=> (tree A B)
                         (list/llist A)))]
  (fun [A B t]
       (r/fix (tree A B)
              (λ [_] (list/llist A))
              t
              (fun [t]
                   (tcase A B t
                          (λ [t] (r/rec (tree A B) (λ [_] (list/llist A)) (list/llist A)))
                          (λ [a] (r/ret (list/lcons A a (list/lnil A))))
                          (λ [ls _ rs]
                             (r/rec-call ls (fun [fls]
                             (r/rec-call rs (fun [frs]
                             (r/ret (list/append A fls frs))))))))))))
